const jwt = require('jsonwebtoken');
const PRIVATE_KEY = process.env.PRIVATE_KEY || 'privateKey';

module.exports = ({ req, res }) => {
  const { body, headers } = req;
  const { authorization } = headers;
  const { operationName } = body;
  if (operationName !== 'authenticate') {
    jwt.verify(authorization, PRIVATE_KEY, (err, token) => {
      if (err || !token) res.status(401).send('not authorized');
      else global.token = authorization;
    });
  }
};
