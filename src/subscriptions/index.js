const { GraphQLObjectType } = require('graphql');


module.exports = new GraphQLObjectType({
  name: 'RootSubscription',
  description: 'Root Subscription',
  fields: {
  }
});
