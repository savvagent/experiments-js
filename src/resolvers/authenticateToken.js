const jwt = require('jsonwebtoken');
const PRIVATE_KEY = process.env.PRIVATE_KEY || 'privateKey';

const user = {
  username: 'user123',
  password: '1234',
  firstName: 'Jon',
  lastName: 'Doe',
  dob: '12/11/1991',
  email: 'user@gmail.com'
};

module.exports = function(root, args) {
  const { username, password } = args;
  if (username === user.username && password === user.password) {
    const token = jwt.sign({ user }, PRIVATE_KEY, { expiresIn: '1h' });
    return Promise.resolve({ token });
  }
  return Promise.reject(new Error('user not recognized'));
};
