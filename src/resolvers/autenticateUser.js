const jwt = require('jsonwebtoken');
const user = {
  username: 'user123',
  password: '1234',
  firstName: 'Jon',
  lastName: 'Doe',
  dob: '12/11/1991',
  email: 'user@gmail.com',
};

module.exports = function(root, args) {
  const { username, password } = args;
  console.log('username', username);
  console.log('password', password);
  if (username === user.username && password === user.password) {
    const token = jwt.sign({ user }, 'privatekey', { expiresIn: '1h' });
    return Promise.resolve({ token });
  }
  return Promise.reject(new Error('user not recognized'));
};
