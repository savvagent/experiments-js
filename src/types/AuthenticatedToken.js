const { GraphQLObjectType, GraphQLString } = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'AuthenticatedToken',
  fields: () => ({
    token: { type: GraphQLString }
  })
});
