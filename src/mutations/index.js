const { GraphQLObjectType } = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'RootMutation',
  description: 'Root Mutation',
  fields: {
  }
});
