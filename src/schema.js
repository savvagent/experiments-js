const { GraphQLSchema } = require('graphql');
const queries = require('./queries/index.js');
const mutations = require('./mutations/index.js');
const subscriptions = require('./subscriptions/index.js');

module.exports = new GraphQLSchema({
  query: queries,
  // mutation: mutations,
  // subscription: subscriptions
});
