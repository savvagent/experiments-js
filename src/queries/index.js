const { GraphQLObjectType } = require('graphql');
const authenticatedUser = require('./authenticatedUser.js');
const authenticate = require('./authenticate.js');

module.exports = new GraphQLObjectType({
  name: 'Query',

  fields: () => ({
    authenticatedUser,
    authenticate
  })
});
