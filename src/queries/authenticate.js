const { GraphQLNonNull, GraphQLString } = require('graphql');

const AuthenticatedToken = require('../types/AuthenticatedToken.js');
const authenticateToken = require('../resolvers/authenticateToken.js');

module.exports = {
  type: AuthenticatedToken,
  description: 'Returns a JSON Web Token with user adata',
  args: {
    username: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) },
  },
  resolve: authenticateToken
};
