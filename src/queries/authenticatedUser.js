const { GraphQLNonNull, GraphQLString } = require('graphql');

const UserType = require('../types/UserType.js');
const authenticateUser = require('../resolvers/autenticateUser.js');

module.exports = {
  type: UserType,
  description: 'Returns a user',
  args: {
    username: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) },
  },
  resolve: authenticateUser
};
