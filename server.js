const { ApolloServer } = require('apollo-server');
const schema = require('./src/schema.js');
const throng = require('throng');
const contextHandler = require('./contextHandler.js');

const PORT = process.env.PORT || 4000;
const WORKERS = 1;

function start() {
  const server = new ApolloServer({
    context: contextHandler,
    schema
  });
  server.listen(PORT, () => console.log(`🚀  Server ready at ${PORT}`));
}

throng({
  workers: WORKERS,
  lifetime: Infinity,
  grace: 4000
},
start);
